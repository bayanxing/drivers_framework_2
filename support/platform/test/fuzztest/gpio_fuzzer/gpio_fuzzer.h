/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */

#ifndef GPIO_FUZZER
#define GPIO_FUZZER

#define FUZZ_PROJECT_NAME "gpio_fuzzer"

enum class ApiNumber {
    NUM_ZERO = 0,
    NUM_ONE,
    NUM_TWO,
};

#endif

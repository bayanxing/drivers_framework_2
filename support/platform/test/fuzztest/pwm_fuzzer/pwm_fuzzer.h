/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */

#ifndef PWM_FUZZER
#define PWM_FUZZER

#define FUZZ_PROJECT_NAME "pwm_fuzzer"

enum class ApiNumber {
    NUM_ZERO = 0,
    NUM_ONE,
    NUM_TWO,
};

#endif
